# Développement

L'objectif est de **développer** les logiciels de la v2.


---
## Avancement / Noeuds


| Logiciel    | Avancement                    | Notes |
|-------------|-------------------------------|-------|
| Duniter v2  | ?                             |       |
| Data pod v2 | Proto IPFS en cours de tests  |       |
| WoT wizard  | ?                             |       |


> À compléter

---
## Avancement / Clients


| Logiciel | Avancement                                                                           | Notes                                                              |
|----------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| gcli     |                                                                                      | Compatibilité avec les anciens Linux, via `Nix` ? (demande de vit) |
| Cesium²  | Ajout du bouton pour certifier; Amélioration des perfs; Passage en API Relay Hasura. | Sera relivrer cette semaine (version `alpha`)                      |
| Gecko    | ?                                                                                    |                                                                    |
| G1nkgo   | V2 non débutée ?                                                                     |                                                                    |
| Autre ?  |                                                                                      |                                                                    |


---

## Rétro-planning

> Exemple fictif (à compléter) 

![Gantt](projects/dev/gantt.svg)
