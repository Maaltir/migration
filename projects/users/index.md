# Accompagnement des utilisateurs

L'objectif est d'**accompagner les utilisateurs** vers la v2

---
## Actions

| Date               | Description                                                                      | Qui ?            |
|--------------------|----------------------------------------------------------------------------------|------------------|
| À planifier        | Besoin d'une démo des outils V2 (Cesium², etc.)                                  | Commission `dev` |
| Rencontres locales | Écouter/Recueillir les besoins des utilisateurs, en termes d'outils pédagogiques |                  |

