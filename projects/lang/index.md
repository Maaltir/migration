# Traduction

L'objectif est de **traduire les éléments** (logiciels, outils de communication, etc.) nécessaires pour la v2

---
## Qui ?

- English - @Junidev (Benjamin) AN
- Deutch - @fab (Fabien) AL
- Spanish - @Aldara
- Italian - @italpaola IT

Chacun renvoie sur la communauté de sa langue les besoins exprimés

---
## Outils

Utilisation de l'outil : https://weblate.duniter.org/

---
## Actions

- (En cours) Constitution des référents traducteurs par langue

- **Prochaine réunion** : vendredi 3 mai 2024 : 20H30 à 22h00
  - Visio : meet.evolix.org/g1v2-lang
  

---

## Avancement

À compléter
